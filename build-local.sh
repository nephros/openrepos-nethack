#BASE_DIR=/home/nephros/devel/sailfish/builder
BASE_DIR=$PWD
BUILD_DIR=$BASE_DIR/build
CACHE_DIR=/home/nephros/.cache/gitlab-runner-cache
mkdir -p $BUILD_DIR

#SFOS_VERSIONS="3.3.0.14 3.4.0.22"
SFOS_VERSIONS="3.4.0.22"
#TARGETS="armv7hl i486"
TARGETS="armv7hl"
DOPTS="--docker-pull-policy=never --docker-volumes "$BUILD_DIR/:/builds:rw" --docker-cache-dir $CACHE_DIR "

echo Want to build everything now?
read dummy
if [[ "$dummy" = "y" ]]; then
    gitlab-runner exec docker $DOPTS --env TARGETS="$TARGETS" --env SFOS_VERSIONS="$SFOS_VERSIONS" .build-local | tee $BUILD_DIR/build.log
    wait
    for sfv in $SFOS_TARGETS; do
      find ../builder/builds/$sfv/*/*/output/ -type f -name "*.rpm"
    done
fi
